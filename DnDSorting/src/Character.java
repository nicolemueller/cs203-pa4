
public class Character implements Comparable<Character>{
	String name;
	Integer skill;
	
	public int compareTo(Character c) {
		return (skill - c.skill);
	}
	
	public Character(String name, Integer skill) {
		this.name = name;
		this.skill = skill;
	}
	public String toString() {
		return (name + "(" + skill + ")");
	}

	
	
	
}
